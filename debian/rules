#! /usr/bin/make -f

# debian/rules
# Part of Debian ‘comixcursors’ package.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

PACKAGE_BUILDDIR = "$(CURDIR)/debian/build"
INSTALLDIR = "$(CURDIR)/debian/tmp"
ICONS_DIR = usr/share/icons
icons_builddir = "${PACKAGE_BUILDDIR}"/${ICONS_DIR}
icons_wrapdir = "${PACKAGE_BUILDDIR}"/etc/X11/cursors
icons_installdir = "${INSTALLDIR}"/${ICONS_DIR}
icons_destdir = "${DESTDIR}"/${ICONS_DIR}

GENERATED_FILES :=

package_mapping = debian/package-mapping
themeset_packages = $(shell cat ${package_mapping} | cut -d':' -f1)

per_package_files = dirs docs install preinst postinst prerm lintian-overrides

INSTALL = install
WRAP_THEMES = "$(CURDIR)"/debian/wrap-cursor-themes


.PHONY: build
build: build-indep build-arch
	dh $@

.PHONY: build-indep
build-indep: build-stamp
	dh $@

.PHONY: build-arch
build-arch: build-stamp
	dh $@

build-stamp: export DESTDIR="${PACKAGE_BUILDDIR}"
build-stamp:
	$(INSTALL) -d "${PACKAGE_BUILDDIR}"


.PHONY: clean
clean:
	$(RM) -r "${PACKAGE_BUILDDIR}"/*
	$(RM) ${GENERATED_FILES}
	dh $@


.PHONY: install
install: build $(foreach package,${themeset_packages},install-${package}-stamp)
	$(INSTALL) -d "${icons_wrapdir}"
	chmod +x ${WRAP_THEMES}
	PACKAGE_BUILDDIR="${PACKAGE_BUILDDIR}" $(WRAP_THEMES)

	dh $@

GENERATED_FILES += install-*-stamp
GENERATED_FILES += $(foreach suffix,${per_package_files}, \
	$(foreach package_name,$(shell cat ${package_mapping} | cut -d':' -f1), \
		debian/${package_name}.${suffix}))
install-%-stamp: themeset_prefix = $(shell cat ${package_mapping} | grep "^$*:" | cut -d':' -f2)
install-%-stamp:
	echo "* Generating files for package ‘$*’ (themeset prefix “${themeset_prefix}”)"
	for n in ${per_package_files} ; do \
		sed \
			-e "s:@THEMESET_PREFIX@:${themeset_prefix}:" \
			-e "s:@PACKAGE@:$*:" \
			debian/templates/$$n.template > debian/$*.$$n ; \
	done
	touch "$@"


.PHONY: binary-indep
binary-indep: build install
	dh $@

.PHONY: binary-arch
binary-arch: build install

.PHONY: binary
binary: build binary-indep binary-arch

.PHONY: override_dh_auto_clean
override_dh_auto_clean:
#	Skip auto-detection of build tool.
	make clean


.PHONY: override_dh_auto_build
override_dh_auto_build:
#	Skip auto-detection of build tool.
	DESTDIR="${PACKAGE_BUILDDIR}" ./install-all
	rmdir "${icons_builddir}"/default

.PHONY: override_dh_auto_test
override_dh_auto_test:
#	Skip auto-detection of build tool.

.PHONY: override_dh_auto_install
override_dh_auto_install:
#	Skip auto-detection of build tool.
	$(INSTALL) -d "${INSTALLDIR}"
	cp -a "${PACKAGE_BUILDDIR}"/. "${INSTALLDIR}"/.

.PHONY: override_dh_installchangelogs
override_dh_installchangelogs:
	dh_installchangelogs NEWS


# Copyright © 2010–2025 Ben Finney <bignose@debian.org>
#
# This is free software; you may copy, modify and/or distribute this work
# under the terms of the GNU General Public License, version 3 or later.
# No warranty expressed or implied.
#
# On Debian systems, the complete text of version 3 of the GNU General
# Public License can be found in ‘/usr/share/common-licenses/GPL-3’.


# Local variables:
# mode: makefile
# coding: utf-8
# End:
# vim: filetype=make fileencoding=utf-8 :
